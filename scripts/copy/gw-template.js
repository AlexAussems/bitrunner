/** @param {NS} ns **/
export async function main(ns) {

	var target = ns.args[0];
	// settings for growth/weaken logic
	var securitythresh = ns.getServerMinSecurityLevel(target) + 5;
	if (!ns.hasRootAccess(target)) {
		// if we made the programs to crack ssh and ftp ports use em before the main nuke() command.
		if (ns.fileExists("BruteSSH.exe", "home")) {
			ns.brutessh(target);
		}
		if (ns.fileExists("FTPCrack.exe", "home")) {
			ns.ftpcrack(target);
		}
		if (ns.fileExists("relaySMTP.exe", "home")) {
			ns.relaysmtp(target);
		}
		if (ns.fileExists("HTTPWorm.exe", "home")) {
			ns.httpworm(target);
		}
		if (ns.fileExists("SQLInject.exe", "home")) {
			ns.sqlinject(target);
		}
		ns.nuke(target);
	}
	//await ns.scp("/scripts/copy/hack-template.script", target);

	while (true) {
		if (ns.getServerSecurityLevel(target) > securitythresh) {
			await ns.weaken(target, { stock: true });
		} else {
			await ns.grow(target, { stock: true });
		}
		await ns.sleep(100);
	}

}