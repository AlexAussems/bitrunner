import { stocktesttargets, hw_script, gw_script } from "/scripts/lib/settings.js"
import { DeployManager, DeployStrategy } from "/scripts/class/DeployManager.js"
import { InstanceInfo, playermoney, currencyFormat, DeploySource } from "/scripts/lib/extras.js"

/** @param {NS} ns **/
export async function main(ns) {

	//getPrice: You don't have TIX API Access! Cannot use getPrice()
	// buy api acces if we do not have that and have enough cash
	if (playermoney(ns) > 100000000000000) {
		// normale tix api acces is blijkbaar eenmalige unlock zie er zo snel geen buy methode voor?
		//	ns.stock.purchase4SMarketDataTixApi(); //25 biljoen
		//	ns.stock.purchase4SMarketData(); // 1 biljoen
	}
	var decimals = 5; // how many decimals to show on the stock price
	// can we affect the stock market test.
	var timeout = 10 * 60 * 1000; // update stats each 10 mins
	// try it on joes guns for now to test
	var trackstock = "JGN"; // stock we need to track
	var trackserver = "joesguns"; // server we are gonne weaken/hack or grow/weaken to see if we can affect the stock price this way.
	// stock stats
	var startstockprice = ns.stock.getPrice(trackstock);
	var curstockprice = startstockprice
	var stockrundiff = 0; //difference in stock price since last update
	// server stats
	var startsrvmonavail = ns.getServerMoneyAvailable(trackserver); // money available on the server
	var srvcurmon = startsrvmonavail;
	var srvmondiff = startsrvmonavail;
	//var srvmondiff = 0; // difference in money since last update
	//var isrunning = true;
	ns.tprintf("START: Server: %s starts at %s available money ", trackserver, currencyFormat(startsrvmonavail));
	ns.tprintf("START: Stock: %s starts at %s ", trackstock, currencyFormat(startstockprice, decimals));

	//await ns.scp(hw_script, "home", trackserver); // copy scriptfile we run on home for now to test
	// create a  run configuration
	// set server that runs it
	var hii = new InstanceInfo(ns, "home"); // deploy to home for testing
	var ds = new DeployStrategy(ns, hii, gw_script, DeploySource.TradeInfluencer);
	var ii = new InstanceInfo(ns, trackserver);
	ds.targets.push(ii);

	//ds.printdebug(ns);
	// deploy the run configuration
	var dm = new DeployManager(ns, ds);
	await dm.deploy(ns)


	// monitor the effect of this action
	var maxcycles = 10;

	// track stats for both instances stock and the server
	for (var i = 0; i < maxcycles; i++) {
		var curmon = ns.getServerMoneyAvailable(trackserver);
		var curprice = ns.stock.getPrice(trackstock);
		srvmondiff = currencyFormat(curmon - srvcurmon);
		stockrundiff = currencyFormat(curprice - curstockprice, decimals);
		// print statistics for this update
		ns.tprintf("Server: %s is now at %s available money %s difference with last update", trackserver, currencyFormat(curmon), srvmondiff);
		ns.tprintf("Stock: %s is now at  %s  %s difference with last update", trackstock, currencyFormat(curprice, decimals), stockrundiff);
		await ns.sleep(timeout);
	}
	// calc final difference
	var curmon = ns.getServerMoneyAvailable(trackserver);
	var curprice = ns.stock.getPrice(trackstock);
	var srvmondifftot = currencyFormat(curmon - startsrvmonavail);
	var stockrundifftot = currencyFormat(curprice - startstockprice, decimals);
	ns.tprintf("TOTAL: Server: %s started at %s available money and now has %s ,  %s difference from the start", trackserver, currencyFormat(startsrvmonavail), curmon, srvmondifftot);
	ns.tprintf("TOTAL: Stock: %s started at  %s and now has  %s ,  %s difference from the start", trackstock, currencyFormat(startstockprice, decimals), currencyFormat(curprice, decimals), stockrundifftot);
	// kill grow/hack scripts
	var ps = ns.ps();
	for (var z = 0; z < ps.length; z++) {
		if (ps[z].filename.indexOf(ds.scriptfile) == 0) {
			ns.kill(ps[z].filename, ds.instanceinfo.name, ps[z].args[0]);
		}
	}

}