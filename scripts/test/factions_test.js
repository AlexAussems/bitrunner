import { fileRead } from "/scripts/lib/extras.js"
/** @param {NS} ns **/
export async function main(ns) {

	var fm = new FactionManager(ns);
	await fm.update(ns);
}



export class FactionManager {
	constructor(ns) {
		this.configfile = "/scripts/config/FactionManager/auto_join_factions.txt";
	}


	async update(ns) {
		// get factions list we want to auto join  from file 
		this.autojoin = await fileRead(ns, this.configfile);
		// source file 6 required for ns.checkFactionInvitations()
		// var factioninvites = ns.checkFactionInvitations(); // get faction invitations
		this.autojoin.forEach(function (faction) {
			//joinFaction: This singularity function requires Source-File 4 to run. A power up you obtain later in the game. It will be very obvious when and how you can obtain it.
			ns.joinFaction(faction);
		});

		await ns.sleep(1000)
	}


}





// export class FactionInfo {
// 	constructor(ns, name, money, town, hlevel = 0, reqrep = 0, backdoor = 0) {
// 		this.name = name;
// 		this.excludes = []; // list of other factions joining this one excludes! 
// 		this.reqmoney = money; // how much money do we need to get an invite
// 		this.reqtown = town; // where do we need to be to get the invite?
// 		this.hlevel = hlevel
// 		this.reqrep = reqrep;
// 		this.backdoor = backdoor;
// 	}


// 	addExcludes(faction) {
// 		this.excludes.push(faction);
// 	}

// 	toExport() {
// 		var sepchar = ";"
// 		var string = "";
// 		string += "name=" + this.name + sepchar;
// 		string += "money=" + this.reqmoney + sepchar;
// 		string += "town=" + this.reqtown + sepchar;
// 		string += "reqrep=" + this.reqrep + sepchar;
// 		for (var i = 0; i < this.excludes.length; i++) {
// 			string += "exclude=" + this.excludes[i] + sepchar;
// 		}
// 		string += "hackinglv=" + this.hlevel + "\n";

// 		//	string += "\n";
// 		//data.push("\n") // newline for readability
// 		return string;
// 	}


// // }