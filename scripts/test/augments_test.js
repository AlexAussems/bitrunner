/** @param {NS} ns **/
export async function main(ns) {
	//getAugmentationsFromFaction: This singularity function requires Source-File 4 to run. A power up you obtain later in the game. It will be very obvious when and how you can obtain it.
	var augs = ns.getAugmentationsFromFaction("Tetrads");
	augs.forEach(function (aug) {
		ns.tprint(aug)
	});
}