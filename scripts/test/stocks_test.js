import { stocktesttargets, hw_script, gw_script } from "/scripts/lib/settings.js"
import { DeployManager, DeployStrategy } from "/scripts/class/DeployManager.js"
import { InstanceInfo, playermoney, currencyFormat, DeploySource } from "/scripts/lib/extras.js"


/** @param {NS} ns **/
export async function main(ns) {

	const COMMISSION = 100000;
	const symbols = ns.stock.getSymbols()
	symbols.forEach(function (sym) {
		ns.tprint(sym);
		var forecast = ns.stock.getForecast(sym);  // 0.6 is 60% chance for it to get better next update
		var price = currencyFormat(ns.stock.getPrice(sym), 4);
		var purchasecost = currencyFormat(ns.stock.getPurchaseCost(sym, 1, "Short"), 4);
		var volatility = ns.stock.getVolatility(sym); // fluctuation of stock each update

		ns.tprintf("%s has a forecast of %s price: %s purchasecost: %s volatility: %s ", sym, forecast, price, purchasecost, volatility);
	});

}





export class StockInfo {
	// https://bitburner.readthedocs.io/en/latest/basicgameplay/stockmarket.html
	constructor(ns, symb) {
		this.ns = ns;
		this.symb = symb;
		this.forecast = ns.stock.getForecast(sym);  // 0.6 is 60% chance for it to get better next update
		this.price = ns.stock.getPrice(sym);
		this.askprice = ns.stock.getAskPrice(sym);
		this.bidprice = ns.stock.getBidPrice(sym);
		this.purchasecost = ns.stock.getPurchaseCost(sym, 1, "Short"); // Short / Long
		this.volatility = ns.stock.getVolatility(sym); // fluctuation of stock each update
		this.remaining = ns.stock.getMaxShares(sym);
	}


}


export class StockInfluencer {
	/**
	 * target a server associated with a company that has stock and see if we can make some money
	 * 
	 */
	TIMEOUT = 1000;
	COMMISSION = 100000;
	PROFIT_THRESHOLD_GROW = 0.4; // WHEN DO WE SELL 40% PROFIT FOR NOW
	PROFIT_THRESHOLD_HACK = -0.4;

	constructor(ns, sym, server, pos = "Long") {
		this.sym = sym; // the stock symbol
		this.server = server; //  server associated with the stock WE HACK/GROW 
		this.moneymade = 0; // total amount we win lose :D
		this.position = pos; //Long / Short
		this.stockstartprice = ns.getPrice(sym);
		this.serverstartmoney = ns.getServerMoneyAvailable(server);
		this.isGrowing = false;
		this.isHacking = false;
		this.isBuying = false;
		this.isSelling = false;
		this.scriptfile;
	}


	async start() {
		if (pos == "Long") {
			this.longLoop(ns);
		} else if (pos == "Short") {
			this.shortLoop(ns);
		}
	}


	async longLoop(ns) {
		this.scriptfile = hw_script;
		// hackserver
		if ((!this.isHacking) && (!this.isGrowing) && (!this.isBuying) && (!this.isSelling)) {
			this.startHacking(ns);
			this.isHacking = true;
		} else if (this.isBuying) {
			// how much can we buy of a stock?
			var amount = 0;
			this.buyStock(ns, this.stock, amount);

		} else if (this.isGrowing) {
			this.startGrowing(ns);


		} else if (this.isSelling) {
			var amount = ns.stock.getPosition(this.sym)[0];
			this.moneymade += ns.stock.getSaleGain(this.sym, amount, "Long");
			this.sellStock(this.sym, amount);
		}
		await ns.sleep(TIMEOUT);
		// threshold of startprice vs current price if we reach that we stop hacking
		var curmon = ns.getServerMoneyAvailable(this.server);
		var curstock = ns.stock.getPrice(this.stock);
		var peronemon = this.serverstartmoney / 100;
		var peronestock = this.stockstartprice / 100;
		// get % increase/decrease since last run 
		var monperc = (curmon - this.serverstartmoney) / peronemon;
		var stockperc = (curstock - this.stockstartprice) / peronestock;
		if ((monperc < PROFIT_THRESHOLD_HACK) && (stockperc < PROFIT_THRESHOLD_HACK)) { // prices needs to drop here so the lower the better
			this.stopHacking(ns);
			this.isBuying = true
		}

		// buy stock
		// growserver
		// sell stock
		//	await ns.sleep(TIMEOUT);
	}

	async shortLoop(ns) {
		// growserver?
		// buy stock
		// hackserver
		// sell stock
		await ns.sleep(TIMEOUT);
	}


	async startHacking(ns) {
		// deploy the hacking scripts and run them 
		var ds = new DeployStrategy(ns, this.getiiHome(ns), hw_script, DeploySource.TradeInfluencer);
		var ii = new InstanceInfo(ns, this.server);
		ds.targets.push(ii);
		// deploy the run configuration
		var dm = new DeployManager(ns, ds);
		await dm.deploy(ns)
	}

	async stopHacking(ns) {

	}

	async startGrowing(ns) {
		// deploy grow scripts after we bought stocks
		var ds = new DeployStrategy(ns, this.getiiHome(ns), gw_script, DeploySource.TradeInfluencer);
		var ii = new InstanceInfo(ns, this.server);
		ds.targets.push(ii);
		// deploy the run configuration
		var dm = new DeployManager(ns, ds);
		await dm.deploy(ns)

	}

	async stopGrowing(ns) {

	}


	async killproces(ns) {
		var ps = ns.ps();
		for (var i = 0; i < ps.length; i++) {
			if (ps[i].filename.indexOf(ds.scriptfile) == 0) {
				ns.kill(ps[i].filename, this.server, ps[i].args[0]);
			}
		}

	}

	buyStock(ns, sym, amount) {
		var maxshares = ns.stocks.getMaxShares(sym)
		var availshares = maxshares; // have to get available shares here
		if (amount <= availshares) { // need to check if some are sold already we need available
			// buy stock
			var buyamount = ns.stocks.getPurchaseCost(sym, amount, "Long");
			if (this.canAfford(buyamount)) {
				return ns.buy(sym, amount);
			}
		}
	}

	sellStock(ns, sym, amount) {
		return ns.stock.sell(sym, amount);
	}

	canAfford(ns, amount) {
		if (playermoney() > amount) {
			return true;
		}
		return false;
	}

	getiiHome(ns) {
		return new InstanceInfo(ns, "home");
	}

}

export class StockStrategy {
	/**
	 * short = buy for it to drop is probably grow server , buy shares at and start hacking the account till it drops then sellShort()
	 * long = buy for it to rise  is probably hack server , buy shares then grow() until we sell again .
	 */

	constructor(ns) {

	}

}

// enum for 
// export class StockStrategy {
// 	// enum class for deploy types
// 	static StockLong = Symbol("StockLong");
// 	static StockShort = Symbol("StockShort");
// 	constructor(name) {
// 		this.name = name;
// 	}
// }