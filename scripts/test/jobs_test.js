/** @param {NS} ns **/
export async function main(ns) {
	ns.joinFaction()

	ns.workForFaction();

	ns.applyToCompany();
	ns.workForCompany()
}