export function playermoney(ns) {
	/**
	 * Get the Players Money
	 */
	return ns.getServerMoneyAvailable("home");
}


export function currencyFormat(number, digits = 2) {
	return number.toLocaleString("en-US",
		{
			style: "currency",
			currency: "USD",
			//			minimumIntegerDigits: digits
			minimumFractionDigits: digits
		})
}

// move this to an enum file at some point?
export class Strategy {
	// enum class for deploy types
	static HighestValueOnly = Symbol("HighestValue");
	static AllValueLB = Symbol("AllValueLB");
	constructor(name) {
		this.name = name;
	}
}


// source of deployment for deploy logic excludes/includes 
export class DeploySource {
	// enum class for deploy types
	static HgwAutoDiscovery = Symbol("HgwAutoDiscovery");
	static TradeInfluencer = Symbol("TradeInfluencer");
	constructor(name) {
		this.name = name;
	}
}




export function isHackable(ns, instanceinfo) {
	// is the instance hackable or do we already have access?
	var phlv = ns.getHackingLevel();
	var insthlv = instanceinfo.ServerRequiredHackingLevel;
	//var phlv = instanceinfo.HackingLevel;
	//	var reqports = instanceinfo.ServerNumPortsRequired;

	//	darkweb null 5 null
	//ns.tprint(instanceinfo.name + " has ports: " + reqports + " hlv: " + insthlv + " - player tools: " + getHackTools(ns) + " phacklv: " + phlv + " RETURN: " + ((phlv > insthlv) && (getHackTools(ns) >= reqports)));
	if (instanceinfo.hasRootAccess) {
		//	ns.tprintf("%s root access ishackable or alreaady hacked", instanceinfo.name);
		return true;
	}
	return ((phlv > insthlv) && canNuke(ns, instanceinfo));
}


export function canNuke(ns, instanceinfo) {
	var reqports = instanceinfo.ServerNumPortsRequired;
	//	var phlv = ns.getHackingLevel();
	return ((instanceinfo.hasRootAccess) || (getHackTools(ns) > reqports));

}


export function crack(ns, instanceinfo) {
	if (!instanceinfo.hasRootAccess) {
		// if we made the programs to crack ssh and ftp ports use em before the main nuke() command.
		var instance = instanceinfo.name;
		//ns.tprint("cracking: " + instance);
		if (ns.fileExists("BruteSSH.exe", "home")) {
			ns.brutessh(instance)
		}
		if (ns.fileExists("FTPCrack.exe", "home")) {
			ns.ftpcrack(instance);
		}
		if (ns.fileExists("relaySMTP.exe", "home")) {
			ns.relaysmtp(instance);
		}
		if (ns.fileExists("HTTPWorm.exe", "home")) {
			ns.httpworm(instance);
		}
		if (ns.fileExists("SQLInject.exe", "home")) {
			ns.sqlinject(instance);
		}
		ns.nuke(instance);
	}
	//installBackdoor: This singularity function requires Source-File 4 to run. A power up you obtain later in the game. It will be very obvious when and how you can obtain it.
	//ns.installBackdoor(instanceinfo.name);
}


export function getHackTools(ns) {
	// determine wich hack tools we have available
	// if we made the programs to crack ssh and ftp ports use em before the main nuke() command.
	var providercount = 0;
	if (ns.fileExists("BruteSSH.exe", "home")) {
		providercount++;
	}
	if (ns.fileExists("FTPCrack.exe", "home")) {
		providercount++;
	}
	if (ns.fileExists("relaySMTP.exe", "home")) {
		providercount++;
	}
	if (ns.fileExists("HTTPWorm.exe", "home")) {
		providercount++;
	}
	if (ns.fileExists("SQLInject.exe", "home")) {
		providercount++;
	}
	//ns.tprint("****Player Hacking tools level: " + providercount);
	return providercount;
}


export class InstanceInfo {
	// holds info about a instance like cash cpu etc so we do not spam static data
	constructor(ns, instance) {
		//	ns.tprint("");
		//ns.tprint("create ii for : " + instance);
		// just get all data for now
		this.name = instance;
		this.hasRootAccess = ns.hasRootAccess(instance);
		this.HackingLevel = ns.getHackingLevel(instance);
		this.ServerMoneyAvailable = ns.getServerMoneyAvailable(instance);
		this.ServerMaxMoney = ns.getServerMaxMoney(instance);
		this.ServerSecurityLevel = ns.getServerSecurityLevel(instance);
		this.ServerMinSecurityLevel = ns.getServerMinSecurityLevel(instance);
		this.ServerRequiredHackingLevel = ns.getServerRequiredHackingLevel(instance);
		this.ServerNumPortsRequired = ns.getServerNumPortsRequired(instance);
		this.ServerMaxRam = ns.getServerMaxRam(instance);
		this.ServerUsedRam = ns.getServerUsedRam(instance);
		//	this.printdebug(ns);
	}

	ServerFreeMem() {
		return (this.ServerMaxRam - this.ServerUsedRam);
	}
	getMaxMoney() {
		return this.ServerMaxMoney;
	}

	printdebug(ns) {
		ns.tprint("Name: " + this.name);
		ns.tprint("Root: " + this.hasRootAccess);
		ns.tprint("HackingLevel: " + this.HackingLevel);
		ns.tprint("ServerMoneyAvailable: " + this.ServerMoneyAvailable);
		ns.tprint("ServerMaxMoney: " + this.ServerMaxMoney);
		ns.tprint("ServerSecurityLevel: " + this.ServerSecurityLevel);
		ns.tprint("ServerMinSecurityLevel: " + this.ServerMinSecurityLevel);
		ns.tprint("ServerRequiredHackingLevel: " + this.ServerRequiredHackingLevel);
		ns.tprint("ServerMaxRam: " + this.ServerMaxRam);
		ns.tprint("ServerUsedRam: " + this.ServerUsedRam);
	}

	compare(a, b) {
		return (a.ServerMaxMoney < b.ServerMaxMoney) ? -1 : (a.ServerMaxMoney > b.ServerMaxMoney) ? 1 : 0;
	}
}



/**
 * File functions
 * 
 */
export async function fileRead(ns, filename) {
	// Read content from a file each object is seperated by a , so with tyhe split() we get a nice list of individual objects
	return await ns.read(filename).split(",");
}


export async function fileWrite(filename, content, mode = "w") {
	return await ns.write(filename, content, mode);
}