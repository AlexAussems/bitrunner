import { Strategy } from "/scripts/lib/extras.js"
// scriptnames
export var copy_hack_script = "/scripts/help/copy_scripts.js"; //script to copy the hgw_script_ to instances				
export var hgw_script = "/scripts/copy/hack-template.js"; // hack grow weaken script
export var hw_script = "/scripts/copy/hw-template.js"; // hack weaken rotation only
export var gw_script = "/scripts/copy/gw-template.js"; // grow weaken rotation only

/**
 * Deployment strat
 * 
 * Strategy.HighestValueOnly: all nodes target the same high value target
 * Strategy.AllValueLB: all nodes load balance all found money sources
 */
export var deploystrategy = Strategy.AllValueLB //Strategy.HighestValueOnly , Strategy.AllValueLB;

// how much room do we want to leave on home for scripts
export var home_use_percentage = 0.8; // use 80% for hgw auto scripting

// Hosts we want to exclude from the automatic hack/weak/grow script as hack target
export var exclude_from_hgw_script = ["joesguns"]; // joesguns is used for my stock influence test


//what stock targets do we want to focus in our testscript?
export var stocktesttargets = []; // implement still unused atm