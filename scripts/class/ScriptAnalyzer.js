import { currencyFormat } from "/scripts/lib/extras.js"

export class ScriptAnalyzer {

	// gets the xp and money gain for a script on an instance for reporting purposes 
	constructor(ns) {
		this.scripts = [];
	}
	/**
	 * └ Threads:
	65
	└ Args: ["phantasy"]
	└ Online Time:
	33 minutes 30 seconds
	└ Offline Time:
	0 seconds
	└ Total online production:
	$276.416m
	 68.547k hacking exp
	└ Online production rate:
	$137.520k / sec
	 34.103 hacking exp / sec
	└ Total offline production:
	$0.000
	 0.000 hacking exp
	
	[01/11/2022 09:29:03] /scripts/debug/debug_scripts.js: Best Money Gainers: 
	[01/11/2022 09:29:03] 0 - /scripts/copy/hack-template.js with args: phantasy and 65 threads has $142,864.81 moneygain
	 
	[01/11/2022 09:29:03] 10 - /scripts/copy/hack-template.js args : phantasy threads: 65 has 34 exp gain
	 */

	scan(ns, target) { // target here so we can possible scan multiple nodes and combine results
		var ps = ns.ps(target);
		ns.tprint("process count: " + ps.length);
		for (var i = 0; i < ps.length; i++) {
			var sr = new ScriptResult(ns, ps[i], target);
			this.scripts.push(sr);
		}
	}

	printHighMoney(ns) {
		ns.tprint("");
		var sorted = this.scripts.sort(function (a, b) { return b.moneygain - a.moneygain });
		ns.tprint("Best Money Gainers: ");
		var i = 0;
		sorted.forEach(function (script) {
			ns.tprintf("%d - %s with args: %s and %d threads has %s moneygain/s", i, script.scriptname, script.args[0], script.threads, currencyFormat(script.moneygain));
			i++;
		});
	}

	printHighExp(ns) {
		ns.tprint("");
		var sorted = this.scripts.sort(function (a, b) { return b.expgain - a.expgain });
		ns.tprint("Best Exp Gainers");
		var i = 0;
		sorted.forEach(function (script) {
			ns.tprintf("%d - %s args : %s threads: %d has %d exp gain/s", i, script.scriptname, script.args[0], script.threads, script.expgain);
			i++;
		});
	}

}


export class ScriptResult {
	// stores the results of script exp gain and money gain for reporting purposes
	/**
	 * /scripts/test_ps_actions.js: [
	 * {"filename":"/scripts/start-influence-stock.js","threads":1,"args":[],"pid":1947},
	 * {"filename":"/scripts/copy/gw-template.js","threads":4463,"args":["joesguns"],"pid":1948},
	 * {"filename":"/scripts/test_ps_actions.js","threads":1,"args":[],"pid":1950}]
	 */
	constructor(ns, proces, target) {
		this.target = target;
		this.scriptname = proces.filename;
		this.threads = proces.threads;
		this.args = proces.args;
		this.pid = proces.pid;
		this.calc(ns);
	}

	calc(ns) {
		// calculate values
		this.expgain = ns.getScriptExpGain(this.scriptname, this.target, this.args[0]);
		this.moneygain = ns.getScriptIncome(this.scriptname, this.target, this.args[0]);
	}

	printdebug(ns) {
		ns.tprint("")
		// todo: de rest

	}
}