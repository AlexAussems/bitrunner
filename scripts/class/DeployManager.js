import { Strategy, DeploySource } from "/scripts/lib/extras.js"
import { copy_hack_script, hgw_script, deploystrategy, home_use_percentage, exclude_from_hgw_script } from "/scripts/lib/settings.js"


export class DeployManager {
	// pushes config scanresults to a runner using the DeployStrategy
	constructor(ns, deploystrategy) {
		this.strategy = deploystrategy; //  what to deploy in what way one/multiple target
		//	ns.print("Deploymanager constructor for " + deploystrategy.instanceinfo.name);
	}

	async deploy(ns, limit = 10) {
		// deploy the strategy to the host depending on setting
		var scriptmem = ns.getScriptRam(this.strategy.scriptfile, this.strategy.instanceinfo.name);

		var target = this.strategy.instanceinfo.name;
		await ns.scp(this.strategy.scriptfile, "home", target);
		if (scriptmem < 1) {
			scriptmem = 3; // think it is 2,8gb atm to run 
		}
		var serveravailablemem = 0;
		if (target != "home") { // do not kill on home!
			if (ns.killall(target)) {
				//	ns.tprint("killAll() OK on : " + this.strategy.instanceinfo.name);
			}
			serveravailablemem = ns.getServerMaxRam(target) // - ns.getServerUsedRam(target));
		} else {
			this.cleanoldprocs(ns, target, this.strategy.scriptfile);
			serveravailablemem = (ns.getServerMaxRam(target) - ns.getServerUsedRam(target)) * home_use_percentage; // keep some free room on home
		}
		//var serveravailablemem = this.strategy.instanceinfo.ServerFreeMem()
		//	ns.tprint(this.strategy.instanceinfo.name + " scriptmem: " + scriptmem + " instanceram: " + serveravailablemem);
		var threads = 1
		switch (deploystrategy) {
			case Strategy.AllValueLB:
				//	ns.tprint("all values")
				threads = ((serveravailablemem / scriptmem) / limit);
				break;
			case Strategy.HighestValueOnly:
				//	ns.tpint("highest only")
				threads = (serveravailablemem / scriptmem);
				break;
		}
		if (threads < 1) { // if we have many targets we just try to start a thread early game this is kinda needed
			threads = 1;
		}
		//	ns.tprint("threads:  " + threads);
		await this.startscript(ns, this.strategy.instanceinfo.name, threads, this.strategy.targets, this.strategy.scriptfile, this.strategy.deploysource, limit);
	}


	async startscript(ns, target, threads, targets, scriptfile, source, limit = 10) {
		//	ns.tprintf("target: %s threads: %d hack  targets: %d", target, threads, targets.length);
		for (var i = 0; i < limit; i++) {
			if ((exclude_from_hgw_script.indexOf(targets[i].name) == -1) || (source != DeploySource.HgwAutoDiscovery)) { // do not hack trade test targets only when we have a trade deployment!
				var curitem = targets[i].name;
				//	ns.tprintf(" target: %s threads: %d hack target: %s", target, threads, curitem);
				ns.exec(scriptfile, target, threads, curitem); // start script
			}
		}
	}

	cleanoldprocs(ns, target, scriptfile) {
		//ns.tprint("cleanoldprocs called")

		var procs = ns.ps(target);
		//ns.tprint(target + " has procs: " + procs.length);
		for (var i = 0; i < procs.length; i++) {

			if (procs[i].filename.indexOf(scriptfile) == 0) {
				//	ns.tprint("Kill() " + procs[i].pid + " - " + procs[i].filename + " -  " + procs[i].args);
				ns.kill(scriptfile, target, procs[i].args[0]); // args 0 for now maybe turn this into a loop, if we define more hosts to one script? do i even want that?
			}
		}
	}
}






export class DeployStrategy {
	// holds what to do and what to target to use
	constructor(ns, ii, scriptfile = hgw_script, source = DeploySource.HgwAutoDiscovery) {
		this.targets = [];
		this.method = deploystrategy; // default value
		this.instanceinfo = ii;
		this.scriptfile = scriptfile; // default value
		this.deploysource = source; // what proces made this deployment?
	}

	printdebug(ns) {
		ns.tprint("")
		ns.tprint("Job Runner: " + this.instanceinfo.name);
		ns.tprint("Targets:");
		ns.tprint("Strategy: " + this.method.toString());
		ns.tprint("Scriptfile: " + this.scriptfile);
		var i = 0;
		this.targets.forEach(function (endpoint) {
			ns.tprintf("%d - %s", i, endpoint.name);
		});
		ns.tprint("Strategy: " + this.method.toString());
		ns.tprint("Scriptfile: " + this.scriptfile);
		this.instanceinfo.printdebug(ns);
	}

}