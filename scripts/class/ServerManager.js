import { currencyFormat } from "/scripts/lib/extras.js"
//
export class ServerManager {
	// will manage buying and destroying/upgrading of purchase servers
	/**
	 * [01/11/2022 10:07:00] 2 memory costs:$ 110,000  
	   [01/11/2022 10:07:00] 4 memory costs:$ 220,000 
	   [01/11/2022 10:07:00] 8 memory costs:$ 440,000 
	   [01/11/2022 10:07:00] 16 memory costs:$ 880,000 
	   [01/11/2022 10:07:00] 32 memory costs:$ 1,760,000 
	   [01/11/2022 10:07:00] 64 memory costs:$ 3,520,000 
	   [01/11/2022 10:07:00] 128 memory costs:$ 7,040,000 
	   [01/11/2022 10:07:00] 256 memory costs:$ 14,080,000 
	   [01/11/2022 10:07:00] 512 memory costs:$ 28,160,000 
	   [01/11/2022 10:07:00] 1024 memory costs:$ 56,320,000 
	   [01/11/2022 10:07:00] 2048 memory costs:$ 112,640,000 
	   [01/11/2022 10:07:00] 4096 memory costs:$ 225,280,000 
	   [01/11/2022 10:07:00] 8192 memory costs:$ 450,560,000 
	   [01/11/2022 10:07:00] 16384 memory costs:$ 901,120,000 
	   [01/11/2022 10:07:00] 32768 memory costs:$ 1,802,240,000 
	   [01/11/2022 10:07:00] 65536 memory costs:$ 3,604,480,000 
	   [01/11/2022 10:07:00] 131072 memory costs:$ 7,208,960,000 
	   [01/11/2022 10:07:00] 262144 memory costs:$ 14,417,920,000 
	   [01/11/2022 10:07:00] 524288 memory costs:$ 28,835,840,000 
	   [01/11/2022 10:07:00] 1048576 memory costs:$ 57,671,680,000
	 * 
	 */
	constructor(ns) {
		this.ns = ns;
		this.timeoutms = 4 * 60 * 1000; //check interval
		this.maxservers = this.ns.getPurchasedServerLimit();
		this.serverprefix = "pserv-";
		this.isrunning = true;
		this.maxram = Math.pow(2, 20);
		this.minservermem = Math.pow(2, 10);// 1024 memory costs:$ 56,320,000 
		this.serversfullupgraded = 0;
		this.totalspend = 0;
		this.serversbought = 0;
		this.serversupgraded = 0

	}

	async start() {
		/** main loop  */
		//	while (this.isrunning) {
		// buy servers first before we upgrade
		//this.ns.tprint("ns check: " + this.ns.getPurchasedServers().length)
		while (true) {
			if (this.ns.getPurchasedServers().length < this.maxservers) {
				// buy a new server with max specs or fixed for now?
				var ram = this.findServerSpec();
				this.ns.print("ServerManager: Adding a server with ram spec: " + ram);
				this.addserver(ram);

			} else {
				this.checkforupgrades();
				if (this.serversfullupgraded == this.maxservers) {
					this.ns.print("ServerManager: No Upgradable servers available")
				}
			}
			if (this.serversfullupgraded == this.maxservers) {
				this.printStats();
				break;
			}
			await this.ns.sleep(this.timeoutms)
		}
	}

	stop() {
		this.isrunning = false;
	}

	findServerSpec() {
		// find the highest spec server we can buy and deploy it
		for (var i = 20; i >= 1; i--) {
			var memval = Math.pow(2, i);
			if (this.ns.getServerMoneyAvailable("home") > this.ns.getPurchasedServerCost(memval)) {
				if (memval >= this.minservermem) {
					return memval;
				}
			}
		}
		return -1
	}

	checkforupgrades() {
		// check all instances for upgrades 
		//	this.ns.print("checkforupgrades: " + this.ns.getPurchasedServers());
		this.serversfullupgraded = 0;
		var instances = this.ns.getPurchasedServers();
		for (var i = 0; i < instances.length; i++) {
			//this.ns.print("checkforupgrades :" + instances[i]);
			var maxram = this.ns.getServerMaxRam(instances[i]);
			//this.ns.tprint(instances[i] + " maxram smaller than max: " + (maxram < this.maxram) + " maxram: " + maxram + " this.maxram: " + this.maxram);
			if (maxram < this.maxram) {
				// upgrade available can we buy a better one?
				var mem = this.findServerSpec();
				//	this.ns.tprint("found mem config: " + mem);
				if (mem != -1) {
					if (mem > maxram) { // only deploy bigger instance

						this.ns.killall(instances[i]); // kill all scripts
						this.ns.deleteServer(instances[i]); // just delete a new one will be placed according to available money
						this.ns.print("ServerManager: upgrade is deleting: " + instances[i] + " with: " + maxram + "  for a server with " + mem);
						this.addserver(mem);
						this.serversupgraded++;
						break;
					}
				} else {
					this.serversfullupgraded++;
				}
			}
			//	this.ns.print("checkforupgrades ended: ");
		}
	}

	addserver(memory) {
		if (memory != -1) { // findServerSpec returns -1 if no match is found no cash/other reason
			var moneyneeded = this.ns.getPurchasedServerCost(memory);
			if (this.ns.getServerMoneyAvailable("home") > moneyneeded) { // check again for available cash to be shure!
				var hostname = this.ns.purchaseServer(this.serverprefix + memory + "-", memory);
				this.totalspend += moneyneeded;
				this.serversbought++;
				this.ns.run("/scripts/auto-everything.js"); // refresh deploy
				//todo: possibly pass the hostname along to the deploy manager with a strategy
			}
		}
	}


	printStats() {


		this.ns.print("Bought " + this.serversbought + " Servers");
		this.ns.print("Servers Upgraded: " + this.serversupgraded);
		this.ns.print("total spend: " + currencyFormat(this.totalspend));
	}
}