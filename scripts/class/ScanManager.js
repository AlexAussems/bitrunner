import { isHackable, crack, canNuke, Strategy, InstanceInfo } from "/scripts/lib/extras.js"
import { copy_hack_script } from "/scripts/lib/settings.js"



export class ScanManager {
	/**
	 * Scans the network for infectable(Runners)/Money or Hackable instances
	 * 
	 */
	constructor(ns) {
		this.scanresult = new ScanResult(ns);
		// add home to the runnerlist

		this.checkrunner(ns, this.createInstanceInfo(ns, "home"));
	}

	scan(ns, instance) {
		//	ns.tprint("start scanning: " + instance)
		var instances = ns.scan(instance);
		//	ns.tprint("----ScanManager scan length: " + instances.length);
		for (var i = 1; i < instances.length; i++) { // skip the first one to avoid redundancy
			var host = instances[i];
			//ns.tprint(host);
			var instanceinfo = this.createInstanceInfo(ns, host);
			if (!this.scanresult.hasVisited(ns, instanceinfo)) { // not scanned before
				this.scanresult.addVisited(instanceinfo);
				//	ns.tprint("after add visited");
				if (canNuke(ns, instanceinfo)) {
					this.checkrunner(ns, instanceinfo);
				}
				//	ns.tprint("after check money");
				//ns.tprint(host + " scan() isHackable : " + isHackable(ns, instanceinfo));
				if (isHackable(ns, instanceinfo)) {
					this.checkmoney(instanceinfo); // we need to be able to hack it lv wise
					//	ns.tprint("after hackable before crack");
					crack(ns, instanceinfo);
					//	ns.tprint("after crack before check runner");

					//	ns.tprint("after check runner");
					this.scan(ns, host);
				}
			}
		}
		//return scanresult;
	}

	createInstanceInfo(ns, instance) {
		return new InstanceInfo(ns, instance);
	}

	checkrunner(ns, instanceinfo) {
		//	ns.tprint("runner check: " + instanceinfo.name + " maxram " + instanceinfo.ServerMaxRam + " ramok: " + (instanceinfo.ServerFreeMem() > 4) + " maxram!=0: " + (!instanceinfo.ServerMaxRam == 0));
		if (!(instanceinfo.ServerMaxRam == 0)) {
			//	ns.tprint("ADD runner check: " + instanceinfo.name + " maxram " + instanceinfo.ServerMaxRam);
			this.addRunner(instanceinfo);
			//	ns.exec(copy_hack_script, "home", 1, instanceinfo.name); // need to run 2 times now to pickup a new server
		}
	}

	checkmoney(instanceinfo) {
		//	ns.tprint("money check: " + instanceinfo.name + " money: " + instanceinfo.ServerMaxMoney + " type of: " + (typeof instanceinfo.ServerMaxMoney) + " === 0: " + (instanceinfo.ServerMaxMoney === 0));
		//	instanceinfo.printdebug();
		if (!(instanceinfo.ServerMaxMoney == 0) && (instanceinfo.ServerMaxMoney > 100)) {
			this.addMoney(instanceinfo);
		}
	}

	addVisited(instanceinfo) {
		this.scanresult.addVisited(instanceinfo);
	}

	hasVisited(ns, instanceinfo) {
		return this.scanresult.hasVisited(ns, instanceinfo);
	}

	addMoney(instanceinfo) {
		this.scanresult.addMoney(instanceinfo);
	}

	addRunner(instanceinfo) {
		this.scanresult.addRunner(instanceinfo);
	}

}
//
export class ScanResult {
	// scanresult because we want to be able to compare them what changed in the network etc.
	constructor(ns) {
		//	ns.tprint("scanresult constructor");
		this.visitedlist = [];
		this.moneylist = [];
		this.runnerlist = []; // auto add home to runners list maybe have to trottle mem usage later
	}

	addVisited(instance) {
		this.visitedlist.push(instance);
	}

	hasVisited(ns, instance) {
		//	ns.tprint(instance.name + " has visited?: " + (this.visitedlist.indexOf(instance) != -1));
		return (this.visitedlist.indexOf(instance) != -1)
	}

	addMoney(instance) {
		this.moneylist.push(instance);
	}

	addRunner(instance) {
		this.runnerlist.push(instance);
	}

	getVisited() {
		return this.visitedlist;
	}

	getMoney() {
		return this.moneylist;
	}

	printVisited(ns) {
		ns.tprint("");
		ns.tprint("-Visited Nodes")
		var i = 0;
		var sortarr = this.visitedlist.sort(function (a, b) { return (a.name.toLowerCase() < b.name.toLowerCase()) ? -1 : (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : 0 }); // alphabetical
		sortarr.forEach(function (ii) {
			ns.tprintf("%d - %s ", i, ii.name);
			i++;
		})
		ns.tprint("~Visted Nodes")
	}
	printMoney(ns) {
		ns.tprint("");
		ns.tprint("-Money Nodes")
		var sortarr = this.moneylist.sort(function (a, b) { return b.ServerMaxMoney - a.ServerMaxMoney }); // put highest value first
		var i = 0;
		sortarr.forEach(function (ii) {
			ns.tprintf("%d - %s has %d Mil max and %d Mil available", i, ii.name, ii.ServerMaxMoney / 1000000, ii.ServerMoneyAvailable / 1000000);
			i++;
		})
		ns.tprint("~Money Nodes")
	}
	printRunner(ns) {
		ns.tprint("");
		ns.tprint("-Runner Nodes")
		var i = 0;
		//	ns.tprint("firstitem: " + this.runnerlist[0].printdebug());
		var sortarr = this.runnerlist.sort(function (a, b) { return b.ServerMaxRam - a.ServerMaxRam }); // put highest value first
		sortarr.forEach(function (ii) {
			ns.tprintf("%d - %s has %d max and %d used and %d free", i, ii.name, ii.ServerMaxRam, ii.ServerUsedRam, ii.ServerFreeMem());
			i++;
		})
		ns.tprint("~Runner Nodes")
	}
}