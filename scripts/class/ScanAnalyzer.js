import { DeployManager, DeployStrategy } from "/scripts/class/DeployManager.js"
import { ScanManager, ScanResult } from "/scripts/class/ScanManager.js"
import { isHackable, crack, Strategy, InstanceInfo } from "/scripts/lib/extras.js"
import { deploystrategy, exclude_from_hgw_script } from "/scripts/lib/settings.js"

export class ScanAnalyzer {
	// scan result interpretation to deployments
	constructor(ns) {
		//ns.tprint("Scanalyzer constructor");
		this.sm = new ScanManager(ns);
		this.deploys = []; // holds all the stuff we need to deploy and assemble here

	}

	async scan(ns, deploy = true) {
		this.sm.scan(ns, "home");
		this.analyze(ns);
		if (deploy) {
			await this.pushdeploys(ns);
		}
	}

	analyze(ns) {
		// get target(s) to hack
		var target;

		switch (deploystrategy) {
			case Strategy.AllValueLB:
				target = this.sm.scanresult.moneylist;
				//	ns.tprint("analyze all values targetted :" + this.sm.scanresult.moneylist.length);
				break;
			case Strategy.HighestValueOnly:
				//	ns.tprint("analyze Highest value only ");
				var sortarr = this.sm.scanresult.moneylist.sort(function (a, b) { return b.ServerMaxMoney - a.ServerMaxMoney }).slice(0, 1); // put highest value first
				target = sortarr
				break;
		}
		ns.print("analyze scan found: " + this.sm.scanresult.visitedlist.length + " targets");
		ns.print("analyze scan found: " + this.sm.scanresult.moneylist.length + " money targets");
		ns.print("analyze scan found: " + this.sm.scanresult.runnerlist.length + " runner targets");
		// create deploy strat objects for each runner we found
		//	ns.tprint("this.scanresult.runnerlist.length: " + this.scanresult.runnerlist.length)

		for (var j = 0; j < this.sm.scanresult.runnerlist.length; j++) {
			var ii = this.sm.scanresult.runnerlist[j];
			//	ns.tprint("creating deployment for: " + this.sm.scanresult.runnerlist[j].name);
			var dstrat = new DeployStrategy(ns, ii);
			dstrat.targets = target
			dstrat.instanceinfo = ii;
			this.deploys.push(dstrat);
		}
		//	ns.tprintf("Before debug strats ");
		//this.sm.scanresult.printVisited(ns);
		//	this.sm.scanresult.printRunner(ns);
		//	this.sm.scanresult.printMoney(ns);
		ns.print("Created: " + j + " Deploystrats ");
	}

	async pushdeploys(ns) {
		ns.print("start of Deployment");
		for (var i = 0; i < this.deploys.length; i++) {
			var dm = new DeployManager(ns, this.deploys[i]);
			await dm.deploy(ns);
		}
		ns.print("end of Deployment");
	}
}