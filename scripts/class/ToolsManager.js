export class ToolsManager {
	/**
	 * Buy tor router and tools 
	 */
	constructor(ns) {
		this.ns = ns;
		this.timeout = 10 * 60 * 1000; // 10 mins
		this.tools = ["BruteSSH.exe",
			"FTPCrack.exe",
			"relaySMTP.exe",
			"HTTPWorm.exe",
			"SQLInject.exe",
			"ServerProfiler.exe",
			"DeepscanV1.exe",
			"DeepscanV2.exe",
			"AutoLink.exe"]
		//loop();
	}

	async start() {
		while(true){
		this.ns.print("Tools Check");
		this.buyTorRouter();
		await this.ns.sleep(this.timeout)
	}
	}
	buyTorRouter() {
		// get tor router for 200.000$
		if (!this.ns.serverExists("darkweb")) {
			if (playermoney > 200000) {
				this.ns.purchaseTor();
				this.ns.print("ToolsManager: Bought TOR Router");
			}
		}
	}

	buy(toolname) {
		//purchaseProgram: This singularity function requires Source-File 4 to run. A power up you obtain later in the game. It will be very obvious when and how you can obtain it.
		if (!this.ns.fileExists(toolname, "home")) {
			this.ns.purchaseProgram(toolname);
		}
	}


}