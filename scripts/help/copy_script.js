/** @param {NS} ns **/
export async function main(ns) {
	var target = ns.args[0];
	//ns.tprint("calling copy script for : " + target);
	var res = await ns.scp("/scripts/copy/hack-template.js", target);
	if (res) {
		//ns.tprint("copied file calling copy script for : " + target);
	}

}