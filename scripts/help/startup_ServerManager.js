import { ServerManager } from "/scripts/class/ServerManager.js"

/** @param {NS} ns **/
export async function main(ns) {
	// disable logging of info in logs
	ns.disableLog("sleep");
	ns.disableLog("getServerRequiredHackingLevel");
	ns.disableLog("getServerNumPortsRequired");
	ns.disableLog("getServerMaxMoney");
	ns.disableLog("getServerMoneyAvailable");
	ns.disableLog("getServerMaxRam");
	ns.disableLog("getServerUsedRam");
	ns.disableLog("getHackingLevel");
	ns.disableLog("serverExists");
	ns.disableLog("kill");
	ns.disableLog("killall");
	ns.disableLog("exec");
	ns.disableLog("run");
	ns.disableLog("scp");
	ns.disableLog("getServerSecurityLevel");
	ns.disableLog("getServerMinSecurityLevel");
	ns.disableLog("scan");
	// end disable excessive logging 


	var sm = new ServerManager(ns);
	await sm.start(ns);
}