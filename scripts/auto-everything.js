import { ScanAnalyzer } from "/scripts/class/ScanAnalyzer.js"



/** @param {NS} ns **/
export async function main(ns) {

	// disable logging of info in logs
	ns.disableLog("sleep");
	ns.disableLog("getServerRequiredHackingLevel");
	ns.disableLog("getServerNumPortsRequired");
	ns.disableLog("getServerMaxMoney");
	ns.disableLog("getServerMoneyAvailable");
	ns.disableLog("getServerMaxRam");
	ns.disableLog("getServerUsedRam");
	ns.disableLog("getHackingLevel");
	ns.disableLog("serverExists");
	ns.disableLog("kill");
	ns.disableLog("killall");
	ns.disableLog("exec");
	ns.disableLog("run");
	ns.disableLog("scp");
	ns.disableLog("getServerSecurityLevel");
	ns.disableLog("getServerMinSecurityLevel");
	ns.disableLog("scan");
	// end disable excessive logging 
	//
	if (!ns.scriptRunning("/scripts/help/startup_ToolsManager.js", "home")) {
		// needs a sourcefile to use
		//	ns.run("/scripts/help/startup_ToolsManager.js"); // if running does nothing
	}

	if (!ns.scriptRunning("/scripts/help/startup_ServerManager.js", "home")) {//} && (ns.getHackingLevel() >= 200)) {
		ns.run("/scripts/help/startup_ServerManager.js"); // if running does nothing on rerun of this script
	}


	// so the scanning needs to be related to our player hack level in the end i guess
	//start scanning proces
	var sa = new ScanAnalyzer(ns);
	//await sa.scan(ns);
	// if (ns.getHackingLevel() > 200) {
	// 	while (true) {
	// 		await sa.scan(ns);
	// 		await ns.sleep(5 * 60 * 1000)
	// 	}
	// } else {
	await sa.scan(ns);
	//}

}