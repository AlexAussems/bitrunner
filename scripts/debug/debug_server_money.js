import { ScanAnalyzer } from "/scripts/class/ScanAnalyzer.js"
/** @param {NS} ns **/
export async function main(ns) {
	var sa = new ScanAnalyzer(ns);
	await sa.scan(ns, false); // do not deploy
	sa.sm.scanresult.printMoney(ns);
}