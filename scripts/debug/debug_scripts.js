import { ScriptAnalyzer } from "/scripts/class/ScriptAnalyzer.js"

/** @param {NS} ns **/
export async function main(ns) {
	var target = "home";

	var sa = new ScriptAnalyzer(ns);
	sa.scan(ns, target);
	//
	sa.printHighMoney(ns);
	sa.printHighExp(ns);

}