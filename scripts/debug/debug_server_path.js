/** @param {NS} ns **/
export async function main(ns) {

	var searchtarget = ns.args[0];
	var targets = [];
	var visited = [];
	var parents = [];
	if (!ns.serverExists(searchtarget)) {
		ns.tprint("server not found: " + searchtarget);
	} else {
		targets.push("home");
		while (targets.length > 0) {
			var curnode = targets.pop();
			if (!visited.includes(curnode)) {
				if (curnode == searchtarget) {
					break;
				}
				visited.push(curnode);
				var nextnode = ns.scan(curnode);
				for (var i = 0; i < nextnode.length; i++) {
					targets.push(nextnode[i]);
					if (nextnode[i] != "home") {
						var pair = [nextnode[i], curnode];
						parents.push(pair);
					}
				}
			}
		}
		// print path for child/parent combo
		var path = [];
		var i = searchtarget;
		while (i != "home") {
			path.push(i);
			for (var j = 0; j < parents.length; j++) {
				var pair = parents[j];
				if (pair[0] == i) {
					i = pair[1];
					break;
				}
			}
		}
		path.reverse();
		for (var z = 0; z < path.length; z++) {
			ns.tprint(z + " - " + path[z]);
		}
	}
}