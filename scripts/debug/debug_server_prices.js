/** @param {NS} ns **/
export async function main(ns) {

	// List all pruchasable servers and they price in a nice readble list on console
	for (var i = 1; i <= 20; i++) {
		var memory = Math.pow(2, i)
		var cost = ns.getPurchasedServerCost(memory)
		ns.tprintf("%d memory costs:$ %s ", memory, cost.toLocaleString());
	}
}