/** @param {NS} ns **/
export async function main(ns) {
	ns.run("/scripts/auto-everything.js", 1);
	ns.run("/scripts/start-influence-stock.js", 1);
}